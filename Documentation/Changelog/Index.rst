.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


Changelog
--------------

+---------+-----------------+---------------+
| Version | Date            | Changes       |
+=========+=================+===============+
| 1.0.0   | 17 June 2016    | First release |
+---------+-----------------+---------------+