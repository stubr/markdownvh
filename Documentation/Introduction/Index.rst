.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


What does it do?
----------------

This is a fluid viewhelper for the Parsedown class https://github.com/erusev/parsedown.
Allows you to parse markdown formatted text in your fluid template.
A use case is content received by a third party app or a form field.

How to apply in fluid:

    {namespace md=STUBR\Markdownvh\ViewHelpers}

    <md:mdtohtml content="{data}"/>

Hint: Markdown requires newline characters – in PHP, these are only functional in double quote strings
If necessary, prep them with something simple as `$content = str_replace('\n', "\n", $content);`` (cf. the comment in the VH class)