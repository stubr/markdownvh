﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM. Check: ÄÖÜäöüß

.. include:: Includes.txt

.. _start:

========================================================================
Markdownvh
========================================================================

.. only:: html

	:Classification:
			markdownvh

	:Version:
			|release|

	:Language:
			en

	:Description:
		  A fluid viewhelper for markdown formatted content, using the Parsedown class.

	:Keywords:
			markdown, content, parsedown

	:Copyright:
			Sturm und Braem GmbH, based on EXT:parsedown, thanks to Michiel Roos, MaxServ B.V.

	:Author:
			Urs Braem

	:Email:
			ub@sturmundbraem.ch

	:License:
			This document is published under the Open Content License
			available from http://www.opencontent.org/opl.shtml

	:Rendered:
			|today|

	The content of this document is related to TYPO3
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.


	**Table of Contents**

	.. toctree::
		:maxdepth: 5
		:titlesonly:
		:glob:

		Introduction/Index
		Changelog/Index

.. only:: latex

	.. toctree::
		:maxdepth: 5
		:titlesonly:
		:glob:

		Introduction/Index
		Changelog/Index